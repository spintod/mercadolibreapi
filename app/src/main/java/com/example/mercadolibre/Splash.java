package com.example.mercadolibre;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Window;
import android.view.WindowManager;

import com.airbnb.lottie.LottieAnimationView;
import com.example.mercadolibre.view.MercadoLibreDetail;

public class Splash extends AppCompatActivity {
    LottieAnimationView lottieAnimationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Para ocultar el status bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        lottieAnimationView = findViewById(R.id.animationView);
        lottieAnimationView.setAnimation(R.raw.caja);
        lottieAnimationView.playAnimation();
        lottieAnimationView.setRepeatCount(1);

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @SuppressLint("ResoureAsColor")
            @Override
            public void run() {
                Intent intent = new Intent(Splash.this, MercadoLibreDetail.class);
                startActivity(intent);
                finish();
            }
        },2200);
    }
}